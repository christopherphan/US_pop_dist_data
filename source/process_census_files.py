# SPDX-License-Identifier: Apache-2.0
#
# process_census_files.py
#
# Christopher Phan
# 2023-W27
#
import pandas as pd


def tweak_population(df: pd.DataFrame) -> pd.DataFrame:
    """Process the file co-est2022-pop.csv."""
    return (
        df.rename(columns={"County": "_county", "2022-07-01": "_population"})
        .assign(
            population=lambda df_: df_._population.str.replace(",", "").astype("Int64")
        )
        .assign(county=lambda df_: df_._county.apply(lambda x: x[1:]))
    )[["county", "population"]]


def tweak_stats_areas(df: pd.DataFrame) -> pd.DataFrame:
    """Process the file list1_2020.csv."""
    df = df.rename(
        columns={
            "County/County Equivalent": "_county",
            "State Name": "state",
            "CBSA Title": "cbsa_title",
            "Metropolitan/Micropolitan Statistical Area": "_sa_type",
            "CSA Title": "csa",
        }
    )
    return (
        df[df.state != "Puerto Rico"]
        .assign(county=lambda df_: df_._county + ", " + df_.state)
        .assign(
            sa_type=lambda df_: df_._sa_type.apply(
                lambda x: "MSA" if x.split(" ")[0] == "Metropolitan" else "\u03bcSA"
            )
        )
        .assign(stat_area=lambda df_: df_.cbsa_title + " " + df_.sa_type)
        .assign(sa_type=lambda df_: df_.sa_type.astype("category"))
    )[["county", "sa_type", "stat_area", "csa"]]


def tweak_ct(df: pd.DataFrame) -> pd.DataFrame:
    """Extract the Connecticut counties out of cbsa-est2022.csv."""
    df = (
        df.assign(state=lambda df_: df_.NAME.apply(lambda x: x.split(",")[1].strip()))
        .assign(_county=lambda df_: df_.NAME.apply(lambda x: x.split(" ")[-2]))
        .assign(_county_wostate=lambda df_: df_.NAME.apply(lambda x: x.split(",")[0]))
        .assign(county=lambda df_: df_._county_wostate + ", Connecticut")
        .rename(columns={"POPESTIMATE2022": "population"})
    )
    return df[(df.state == "CT") & (df._county == "County,")][["county", "population"]]


def remove_redundant_ct(df: pd.DataFrame) -> pd.DataFrame:
    """Remove the Connecticutt planning regions from the data."""
    return df[~df["county"].str.contains("Planning Region, Connecticut")]


if __name__ == "__main__":

    raw_county_populations = pd.read_csv("raw_data/co-est2022-pop.csv")
    county_pop_prelim = remove_redundant_ct(tweak_population(raw_county_populations))
    raw_stats_areas = pd.read_csv("raw_data/list1_2020.csv")
    stats_areas = tweak_stats_areas(raw_stats_areas)

    ct_data_raw = pd.read_csv("raw_data/cbsa-est2022.csv", encoding="latin-1")
    ct_data = tweak_ct(ct_data_raw)

    county_pop = pd.concat([county_pop_prelim, ct_data]).reset_index(drop=True)

    combo_data_pre = pd.merge(county_pop, stats_areas, on="county", how="outer")

    outside_counties = combo_data_pre[combo_data_pre.sa_type.isna()].assign(
        stat_area=lambda df: df.county, sa_type="outside"
    )

    combo_data_sa_only = combo_data_pre[~combo_data_pre.sa_type.isna()]

    combo_data = pd.concat([combo_data_sa_only, outside_counties])
    combo_data.to_csv("combo_data.csv", index=False)
