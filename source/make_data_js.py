# SPDX-Licence-Identifier: Apache-2.0
#
# make_data_js.py
# 
# Christopher Phan
# 2023-W27
# 
import json
import textwrap


def format_subentry(county_info: dict) -> str:
    return (
        "{\n"
        + "\n".join(f"  {key}: {value!r}," for key, value in county_info.items())
        + "\n},"
    )


def format_entry(key: int, value: list[dict]) -> str:
    return (
        f"{key}: [\n"
        + "\n".join(
            textwrap.indent(format_subentry(subdict), " " * 2) for subdict in value
        )
        + "\n],"
    )


def xor_byte(x: int, y: int) -> int:
    return (x | y) & ~(x & y)


def xor_bytes(x: bytes, y: bytes) -> bytes:
    y_len = len(y)
    return bytes(xor_byte(x[k], y[k % y_len]) for k in range(len(x)))


key: bytes = bytes.fromhex("dead beef baddecaf c0ffee")

spdx_header = xor_bytes(
    bytes.fromhex(
        "f1829ebcea99b4828c968dbbc3cd8a979488caae8b87b8c4db9d80fdafecf0d2dff09d"
    ),
    key,
).decode()

CC_decl = xor_bytes(
    bytes.fromhex(
        "f48794c590f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c685ead5c4f487"
        + "94c590f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c685ead5c4f48794c5"
        + "90f7c685ead5c4f48794c590f7c685ead5c4d48d94cfee95a5fce0b9a792e89ea0f491"
        + "b58fb29a82bbcccd8adefd99c1a49a9ce48dd69bcead9f95efd08dacc8df9bd3ab89cc"
        + "af9283b1c3cdc1d5af8b80b08a8cb2c4dd8bd5b08dc6aed094bbdfd1c08bf3dc80cadf"
        + "c4d48d94cfeeb2ccdba89acebbd5ca8ad4a9ccdfaf8c9db7cfd28a9aa882cba58dceb2"
        + "ccc9c39a9e84dda98c9ab1ddd68ac8fdbcc7a191ceb6cccdcfcdbc85d9a59bcebfc1d2"
        + "cfd9b29cd6b29689b6d9b4cf90fd8dc1a4df9cbbc1df9bdfb9ccc0b2df80bbc4d987d8"
        + "b29ec6ae98ceacc4d987ceaeccdbafdf8abfd9dfc1d0aec28f949787ad8dc980c8b6cc"
        + "c6b3df9eabcfd286c9b589cbe0999cb1c084e59af7ccfaae969abbc99ebccebc98cab3"
        + "d1e4fe879ec590f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c685ead5c4"
        + "f48794c590f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c685ead5c4f487"
        + "94c590f7c685ead5c4f4a79ec59a89a4eae0b9a192e1f1b8f393ab8f81afbe92e4fbbc"
        + "9a89a38f94b7a78d8df8a6f698cce08eb3b7e4a79ec5b0fdc68f838d8bbfd9d799dffd"
        + "afc0ad9281b0de9ea3dfba8dc3e0bc81bac8b4cf90d7cc85e0dfad9d9d9ede94edccfa"
        + "ae9698bbdfcd8ed6d7cc85cadfc4fe8dfdbdff9cb8e696bace9de2f3a2f593bf8f83b0"
        + "bc8ee2ecaeee94a3e1e0b6bdfee3f1bb9a9ccce381a8ce98e4eca29a9ca2ebe0bba19b"
        + "fe9ea1f589ccff92b0b897e9fbcff698abee8cf5cef48d9ebcff8fbae683babdf08dfa"
        + "a6e989bee682aaba97e2f0cff59bccfb88b6bdfee9f1acef90a9e194dfaa91e8edcff4"
        + "92b88f83adab9ff9fbcffb93ccee94aba18ce3fbb6979ea0e685b1bad48d94cf9a8fa9"
        + "e381aba791e3eda7f38dc28f83adab9ff9f7b9fffdafe08db2a190fe9ebfe892bae684"
        + "babdfef9f6a6e9fda5e186b0bc93eceaa6f593cce08edfaf908d9caee9f0a5fce2f5ce"
        + "f48d9eadfb8ea5fceedfad8ce8ffbbf38ba98f83b0a393e2f0bc9a90ade485acce90e2"
        + "9eb8fb8fbeee8eaba79bfe9ebdff9aadfd84b6a0998deaa7fffdb9fc85dfa1988deaa7"
        + "f38ee68feadfce9ae2fdbaf798a2fbe0b0bcfef9f6aa9a94a2e98fada39ff9f7a0f4fd"
        + "a3fde0a8a18ce6edcfea8fa3f989bbab9a8df6aae898b9e184babcf28dffa1fefda8e6"
        + "93bca29fe4f3bcb0fdc68fe0b3a79feff7a3f389b58f86b0bcfee9ffa2fb9aa9fce0ad"
        + "ab8df8f2bbf393ab8f86ada1938deaa7fffdb9fc85dfa1988deaa7f38ecceb8fbcbb93"
        + "e8f0bb9a92be8f94b7abd48d94cf9a94a2e98fada39ff9f7a0f4fda3fde0a8a18ce6ed"
        + "cfea8fa3f989bbab9a8df6aae898b9e184babcf0a79ec5b0fdc68fe0ac9abfd9db82df"
        + "b3988faf99ce8ed8cc9fd5ae89a5e0d5e4fe879ecfeeb5898fac9e99ad8dd1899ab083"
        + "dcb4df84abdfd79cdeb48fdba99080ad8dca87c8b299c8a8909baa8dca87dffd9bc0b2"
        + "938afecccb9bd5b08ddba99c8fb2c1c7cfd9b282c9a58de4fe879ecfdfa58fc3b58c87"
        + "a8c89eacd5ad95dda99886aa8ddf81defdbecaac9e9abbc99ebdd3ba84dbb3dfc6bac8"
        + "d886d4b8888fa29a82b1da97cfcfad83c1e08b86bb8ddd9ddfbc98c0b2df8fb0c9b4cf"
        + "90fdccdcb59d9dbbdccb8ad4a9ccc0b7918bac85cdc69af589cea397cebfc3dacfdbb1"
        + "8083e09e80fe8fd198d4b89e8de9df81b88ddf819ab29ec6a79680bfc19e98d5af878f"
        + "af99e4fe879ecfdba898c7af8d9db6c4cecfdbb38880af8dcebf8dda8ecebc8eceb39a"
        + "cef6c8df8cd2f1cccee0ddb9b1dfd5cd93f3e68feaf5cef48d9eacdfaf98cea991ceb1"
        + "dad08ac8aeccd8a98c86fed9d1cfcab89ec2a1918bb0d9d2969aaf89c3a9919fabc4cd"
        + "879aa984c0b39aceacc4d987ceaeccdbafdf8ffefad19dd1fd8ac0b2f5cef48d9e9bd2"
        + "b8ccdfb58d9eb1dedbcfd5bbccccaf919aacc4dc9aceb482c8e08b81fecc9e8cd5b081"
        + "c0ae8cceb1cb9e8cc8b88ddba9898bf28ddd9ad6a999dda193cebfc3dae59af7cc8fb3"
        + "9c87bbc3ca86dcb48f8fb7909cb5de9ec7989e83c2ad9080ad8f97cfceb58ddbe08b86"
        + "bb8dce9ad8b185cce09c8fb08dcc8ad6b48dcdac86cebfc3dacfcdb498c7af8a9afecb"
        + "db8ec8d7cc85e0df81b88dd28eceb89e8fa3938fb7c0cdcfd5bbccc6ae999cb7c3d98a"
        + "d7b882dbe09d9bb7c1dacfcfad83c1ecdf83b1c9d789c3f1ccc6ae9c81acddd19ddba9"
        + "898fa991ceb1d9d68ac8d7cc85e0df99b1dfd59c96fd9ecab58c8bfeccd08b9aaf89cb"
        + "a98c9aacc4dc9aceb8ccceb3df88acc8db83c3fd8ddce08f81added78dd6b8ccc6aedf"
        + "8fb0d49e89d5af818fb7978faaded18accb89ea5e0d5cefeccd08b9abb83dde09e80a7"
        + "8dce9ac8ad83dca58cc2fec4d08cd6a888c6ae98cea9c4ca87d5a8988fac9683b7d9df"
        + "9bd3b2828fa39083b3c8cc8cd3bc808fb08a9caec2cd8ac9f3e68feadfce8ac5db9cdf"
        + "fd83d8ae9a9cad8dd38ec3fd8fc0ae8b9cb7cfcb9bdffd98c0e08b86bb8dfd80d7b083"
        + "c1b3df9ab18dce9dd5b083dba5df9ab6c89e86deb88dc3e09088fecc9e89c8b889a5e0"
        + "d5cefececb83cea89ecae09e80ba8dca87dffd8adab28b86bbdf9e9fc8b288daa38b87"
        + "b1c39e80dcfd8fdda59e9ab7dbdbc39abe99c3b48a9cbfc19e8ed4b9ccdca3968bb0d9"
        + "d789d3bee68feadfcea9c2cc84c9f1ccc0b2df9ab18dd98ed3b3ccdda58f9baaccca86"
        + "d5b3ccc0b2df89acc8df9bdfafcccba98c9aacc4dc9aceb483c1e09981ac8dca87dfb4"
        + "9e8f97909cb58dd7819aad8dddb4f5cef48d9e9bd2af83daa797ceaac5dbcfcfae898f"
        + "a1918afec8d889d5af98dce09088fec2ca87dfaf9f81cadfc4d48d94cf9a9b83dde08b"
        + "86bbdedbcfdbb38880af8dceb1d9d68ac8fd9cdab28f81adc8cdcfdbb3888fad909ab7"
        + "dbdf9bd3b282dcecdf8fb0c99e98d3a984c0b58bcebfc3c7e59af7cc8fa5879ebbceca"
        + "8eceb483c1e09088feccda8bd3a985c0ae9e82feced181c9b488cab29e9ab7c2d0cfd5"
        + "afccccaf929ebbc3cd8eceb483c1ecdf9ab6c89e9fdfaf9fc0aef5cef48d9e8ec9ae83"
        + "cca99e9ab7c3d9cff99edc8fb7969ab68ddfcfedb29ec4e0d79ab6c89ecdfbbb8ac6b2"
        + "928bac8f97c39aa9838fb4978bfec8c69bdfb3988fb4978faa8dd68a9ab29e8fb3978b"
        + "d48d94cf9ab49f8fa191ceb1dad08ac8fd83c9e0bc81aed4cc86ddb5988fa1918afeff"
        + "db83dba989cbe0ad87b9c5ca9c9ab4828fb4978bfefad19dd1f1ccd9af939bb0d9df9d"
        + "d3b1958fa5938bbdd9cde59af7cc8fb490cebfddce83c3fdafecf0df9ab18dca87dffd"
        + "bbc0b294cebfc3dacfcaa88ec3a99c82a78dda86c9a99ec6a28a9abb8dca87dffdbbc0"
        + "b294ceabc3da8ac8fd85dbb3df9abbdfd39c96d7cc85e0df99b7d9d6cfd1b383d8ac9a"
        + "8ab9c89e80dcfd84c6b3df81ac8dd68ac8fdafc0b0869cb7cad69b9abc82cbe0ad8bb2"
        + "ccca8adefdbec6a7979aad8dd7819aa984cae0a881acc69e8ed4b9e68feadfceaac5db"
        + "cfd7b88dc1a99189feccd08b9ab482dba5918abbc99e83dfba8dc3e09a88b8c8dd9b9a"
        + "b28a8f83bcdefec2d0cfceb583dca5df9cb7cad69bc9f3e68feaf5cef48d9ede94fdaf"
        + "c0b0869cb7cad69b9abc82cbe0ad8bb2ccca8adefdbec6a7979aad839eae9a8a83ddab"
        + "df83bfc9dbcfdbab8dc6ac9e8cb2c89e9ad4b989dde0bcadee8dd38ec3fd8ecacadfc4"
        + "fe8dce9dd5a989ccb49a8afecfc7cfd9b29cd6b29689b6d99e8ed4b9ccdda5938faac8"
        + "dacfd5afccc1a59689b6cfd19dd3b38b8fb29689b6d9cdcf92ffafc0b0869cb7cad69b"
        + "9abc82cbcadfc4fe8dec8ad6bc98caa4dfbcb7cad69bc9ffc581e0bc81aed4cc86ddb5"
        + "988fa1918afeffdb83dba989cbe0ad87b9c5ca9c9ab482ccac8a8abb819e8dcfa9ccce"
        + "b29aceb0c2cae59af7cc8fac9683b7d9db8b9aa98383e08b86bb8dd880d6b183d8a991"
        + "89e4a79ec5b0fdc68fe0dfceb7839e9bd2b8ccdda99886aa8dca809aaf89dfb2908aab"
        + "cedbc39abc88ceb08bc2fec9d79cceaf85cdb58b8bf28dce8ac8bb83ddadd3cebac4cd"
        + "9fd6bc9583cadfc4fe8d9ecf9afdccccaf9283abc3d78cdba98983e09e80ba8dca9ddb"
        + "b39fc3a18b8bfecc9eb8d5af8794cadfc4d48d94cf9afdccc6a9d1ceb3c2cc8ed6fd9e"
        + "c6a7979aad8dcc8acebc85c1a59bcebcd49e9bd2b8ccc0b29689b7c3df839abc99dba8"
        + "909cf6de97cfdbb38880af8dceaec8cc89d5af81cab2d79df796b4cf90d7cc85e0dfce"
        + "fec4d78694fd9cdaa29387bdc4ca969abc82cbe08f9cb7dbdf8cc3fd9ec6a7979aad8d"
        + "ce8ac8a98dc6ae9680b98dca809abcccdfa58d9db1c3999c9ab481cea79aceb1dfb4cf"
        + "90fdcc8fe0dfcefe8d9e83d3b689c1a58c9dfec9db9fd3be98caa4df87b08ddfcfedb2"
        + "9ec4fbf5cef4a79ec59afdcc8fa989c0fedfd788d2a99f8fb08d81aac8dd9bd3b38b8f"
        + "a1988fb7c3cd9b9aa882c9a1969cfeced182cab898c6b49681b08dd7819aaf89c8a18d"
        + "8aad8dca809abcccf8af8d85f2a79ec59afdcc8fe0dfcefedecb8dd0b88fdbe08b81fe"
        + "d9d68a9ab185c2a98b8faac4d181c9fd85c1e08f8facccd99ddbad848ff4d78ff7819e"
        + "8ddfb183d8fbf5cef4a79ec59afdcc8fb6d1ceacc4d987ceaeccdfb2909abbceca86d4"
        + "baccdba89acebbd5ca9ddbbe98c6af91c2fec9d79cc9b881c6ae9e9ab7c2d0c39aa89f"
        + "cae09e80ba8dcc8acfae898faf99cebaccca8eb0fdc68fe0dfcefe8d9e86d4fd8d8f97"
        + "909cb596b4cf90d7cc85e0dfcefedbd7c19ab98ddba19d8fadc89e9dd3ba84dbb3dfc6"
        + "add8dd879abc9f8fb49781adc89e8ec8b49fc6ae98ceabc3da8ac8fda8c6b29a8daac4"
        + "c88a9ae4da80f9d0ab9d8dd1899aa984cacadfc4fe8d9ecf9afdcc8f858a9cb1dddb8e"
        + "d4fdbcceb29387bfc0db81cefd8dc1a4df81b88dca87dffdafc0b5918db7c19e80dcfd"
        + "dd9ee0b28facced6cf8be4d599e09080fed9d68a9ab189c8a193e4fe879ecf9afdcc8f"
        + "e0df9eacc2ca8ad9a985c0aedf81b88dda8ecebc8eceb39a9df28ddf81defd99c1a49a"
        + "9cfeccd0969ab38ddba99080bfc19e86d7ad80caad9a80aaccca86d5b3ccdba89a9cbb"
        + "c2d8c3b0fdc68fe0dfcefe8d9ecfd3b38fc3b59b87b0ca9e8ed4a4cccead9a80bac8da"
        + "cfd5afccdcb59c8dbbdecd80c8fd9acab28c87b1c39e80dcfd9fdaa397cebac4cc8ad9"
        + "a985d9a5d6d5feccd08bb0fdc6a5e0d5cefe8d9e99d3b4c28fe0909ab6c8cccfc9b481"
        + "c6ac9e9cf28ddb9ecfb49aceac9a80aa8dd19d9abe83ddb29a9daec2d08bd3b38b8fb2"
        + "9689b6d9cdcfceb59ec0b59886b1d8cacfceb589a5e0d5cefe8d9ecf9afdcc8fe08881"
        + "acc1dacfd8bc9fcaa4df81b08ddf9fcab185cca19d82bb8dd28ecdfd83dde08b9cbbcc"
        + "ca9696fd8dc1a4df8fb0d49e81dba985c0ae9e82fec4d39fd6b881caae8b8faac4d181"
        + "c9d7cc85e0dfcefe8d9ecf9afdccdba89a9cbbc2d8c1b0fdc6a5e0d5cefe9f90cfedbc"
        + "85d9a58dc0fef9d1cfceb5898fa78d8bbfd9db9ccefd89d7b49a80aa8dce8ac8b085db"
        + "b49a8afecfc7c39abf99dbe09181aa8dd7819abe83c1b48d8fa8c8d09bd3b282a5e0d5"
        + "cefec2d8c39abc9cdfac968dbfcfd28a9ab18dd8ecdfafb8cbd79dd7b89e8fa89a9cbb"
        + "cfc7cfd5ab89ddb49397f28dd89ad6b19583e08f8bacc0df81dfb398c3b9d3e4fe879e"
        + "cfd3af9ecab6908dbfcfd2969abc82cbe08a80bdc2d08bd3a985c0ae9e82b2d49e98db"
        + "b49acab3d3cebfcfdf81deb282dcecdf8fb0c99e9ccfaf9ecaae9b8bacde9e8ed6b1cc"
        + "c0a6f5cef48d9eaedcbb85ddad9a9cf9de9eacd5ad95dda99886aa8ddf81defdbecaac"
        + "9e9abbc99ebdd3ba84dbb3df8fb0c99e8ec9ae83cca99e9abbc99e8cd6bc85c2b3df8f"
        + "b0c99e8cdba89fcab3df81b8a79ec59afd8dccb49681b0819e98d2b898c7a58dceb0c2"
        + "c9cfd1b383d8aedf81ac8dcb81d1b383d8aedfc6b7c3dd83cfb985c1a7df8ba6c4cd9b"
        + "d3b38b8fa18ccea9c8d2839abc9f8fa68a9aabdfdbe59af7cc8fa3938fb7c0cdcfdbb3"
        + "888fa39e9badc8cdcfd5bbcccea38b87b1c397c39ab4828fb4978bfefad19dd1fdc4c6"
        + "e9df87b08ddf83d6fd98cab28d87aac2cc86dfaee68feadfcea9c2cc83deaa85cba5d3"
        + "cef6c4d7c69abb83dde08b86bb8dd38ec2b481daaddf8aabdfdf9bd3b2828fb08d81a8"
        + "c4da8adefd8ed6e09e9eaec1d78cdbbf80cae0938fa98dd19db0fdc68fe08b9cbbccca"
        + "969af585c1a3939bbac4d0889abb99dbb58d8bfed9d782dffd89d7b49a80adc4d181c9"
        + "f4c08fe89687b7849e86d4fd8dc1b9df8dabdfcc8ad4a9ccc0b2df88abd9cb9ddfd7cc"
        + "85e0df83bbc9d79ad7fd8dc1a4df88b1df9e8ed4a4ccc1b5928cbbdf9e80dcfd8fc0b0"
        + "968bad819e8ed4b9cc87a989c7fecbd19d9abc82d6e08f9bacddd19cdffd9bc7a18b9d"
        + "b1c8c88ac8f1e68feadfceb7c3dd83cfb985c1a7df99b7d9d680cfa9ccc3a99287aacc"
        + "ca86d5b3ccccaf9283bbdfdd86dbb1c08fa19b98bbdfca86c9b482c8e0909cfeddcc80"
        + "d7b298c6af918fb2a79ec59afd9cdab28f81adc8cdcf92a984cae0ddb9bfc4c88ac8ff"
        + "c581e0be88b8c4cc82dfafccc2a1948bad8dca87dffdbbcea9898bac8dd880c8fd98c7"
        + "a5df8cbbc3db89d3a9ccc0a6df8bbfced6e59af7cc8fad9a83bcc8cccfd5bbccdba89a"
        + "ceaed8dc83d3beccceb4df82bfdfd98a9abc82cbe08b81fed9d68a9ab989dbb29683bb"
        + "c3cacfd5bbcceea69987acc0db9d9daeccc7a5969cad8ddf81ded7cc85e0df9dabcedd"
        + "8ac9ae83ddb3d3ceb8d8d283c3fd85c1b49a80bac4d0889aa984ceb4df9dabced6cfed"
        + "bc85d9a58dceadc5df83d6fd82c0b4df8cbb8dcd9ad8b789ccb4df9ab1a79ec59afd9e"
        + "cab6908dbfd9d780d4f1ccdda58c8db7decd86d5b3c08fa39e80bdc8d283dba985c0ae"
        + "d3ceaac8cc82d3b38ddba99080f28dd19d9abc82d6e0909ab6c8cccfd6b88bceacdf81"
        + "aca79ec59afd89deb5969abfcfd28a9abc8fdba99080fed9d1cfdeb49fddb58f9afed9"
        + "d68a9aac99c6a58bcebbc3d480c3b089c1b4df81b88dca87dffdbbc0b294cebcd49e9b"
        + "d2b8ccdfb59d82b7ceb4cf90fdccceb3df8db1c3ca8ad7ad80ceb49a8afecfc7cffbbb"
        + "8ac6b2928bac8acdcfdfa59cdda58c9dfefeca8eceb881caae8bceb1cb9ebfcfaf9cc0"
        + "b39ac0d48d94e59af7cc8ff3d1ce8ed8dc83d3becce3a99c8bb0dedbcffcbc80c3a29e"
        + "8db5839ebcd2b299c3a4df8fb0d49e9fdbaf988faf99ceaac5dbcfedbc85d9a58dceb8"
        + "c2cccfdbb3958fb29a8fadc2d0cfd8b8e68feadfceb4d8da88dfb9ccc3a5988fb2c1c7"
        + "cfd3b39aceac968afec2cccfd3b389c9a69a8daac4c88a9aa882cba58dcebfddce83d3"
        + "be8dcdac9aceb2ccc9c39aa984caaedf9ab6c89eb8dbb49acab2f5cef48d9e9cd2bc80"
        + "c3e09d8bfeddcc8ac9b89ed9a59bceaac29e9bd2b8ccc2a18787b3d8d3cfdfa598caae"
        + "8bceaec8cc82d3a998caa4df9abfc6d781ddfd85c1b490cebfcedd80cfb398a5e0d5ce"
        + "feecd889d3af81cab2d89dfec8c69fc8b89fdce0ac9abfd9db82dfb3988faf99ce8ed8"
        + "cc9fd5ae8981e0b680feccda8bd3a985c0aed3ceaac29e9bd2b8cccab88b8bb0d99e9b"
        + "d2b8e68feadfce89ccd799dfafccc6b3df9db18dd49adeba89cbe0be88b8c4cc82dfaf"
        + "ccc7a58d8bbcd49e88c8bc82dbb3df9ab18ddb8ed9b5cccea6998bbdd9db8b9aad89dd"
        + "b39080feccb4cf90fdccddaf868fb2d9c7c2dcaf89caecdf80b1c39e9bc8bc82dca69a"
        + "9cbfcfd28a96fd82c0aedf9dabcfd286d9b882dca19d82bb819e81d5b3cccab89c82ab"
        + "ded799dff1e68feadfceb7dfcc8accb28fcea2938bfeccd08b9aa882ccaf918ab7d9d7"
        + "80d4bc808fac968dbbc3cd8a9aa9838fa5878bacced79cdffdadc9a6969cb3c8ccc8c9"
        + "fdafc0b0869cb7cad69b9abc82cbcadfc4fe8dec8ad6bc98caa4dfbcb7cad69bc9fd85"
        + "c1e08b86bb8de980c8b6cc87a9d6ceb7c39e8ed6b1ccdba58d9cb7d9d19dd3b89f8fb7"
        + "909cb2c9c986deb8c08fe89687f78dd880c8fd98c7a5f5cef48d9e82dba585c2b592ce"
        + "bad8cc8eceb483c1e08f9cb1dbd78bdfb9cccdb9df8faeddd286d9bc8ec3a5df82bfda"
        + "9e80c8fd98dda59e9aa78d9686d4be80daa49680b98dd89acea89ecacadfc4fe8dca86"
        + "d7b8cccab88b8bb0ded780d4aec583e0d787b7c497cfd3b3ccceae86cebdd8cc9ddfb3"
        + "988faf8dceb8d8ca9ac8b8ccc2a59b87abc09e8ed4b9ccc9af8dcebfc3c7e59af7cc8f"
        + "ae8a83bcc8cccfd5bbccccaf8f87bbde92cfdbb3888fe89698f78dd880c8fd8dc1b9df"
        + "9eabdfce80c9b8ccd8a89e9aadc2db99dfafc08fa9918db2d8da86d4baccd8a98b86b1"
        + "d8cae59af7cc8fac9683b7d9df9bd3b2828fa39083b3c8cc8cd3bc8083e09e8aa8c8cc"
        + "9bd3ae85c1a7df81ac8dce9dd5b083dba99080bfc19e9fcfaf9cc0b39a9dfe85ca87df"
        + "fdcee3a99c8bb0dedbcd93f3e68feadfce8ac5dbcff6b48fcaae8c8bfeded68ed6b1cc"
        + "cda5df8abbc8d38adefd89c9a69a8daac4c88a9abc9f8faf99ceaac5dbcfdebc98cae0"
        + "bcadee8dc98ec9fd8ddfb09387bbc99e8dc3d7cc85e0dfafb8cbd79dd7b89e8fb490ce"
        + "aac5dbcfedb29ec4eedfbdb6c2cb83defd8dc1b9df9ebfdfcacfd5bbccdba89ace92c4"
        + "dd8ad4ae898fa6909cfeccd0969aaf89ceb39080fecfdbe59af7cc8faa8a8ab9c8dacf"
        + "d6b88bceac9397fec4d099dbb185cbe0909cfec4d08adcbb89ccb49698bb8dcb81deb8"
        + "9e8fa18f9eb2c4dd8ed8b1898fac9e99f28dcd9ad9b5ccdfa18d9ab7ccd2e59af7cc8f"
        + "a99198bfc1d78bd3a9958faf8dceb7c3db89dcb88fdba9898bb0c8cd9c9aae84ceac93"
        + "ceb0c2cacfd3b39aceac968abfd9dbcfceb5898fb29a83bfc4d08bdfafccc0a6df9ab6"
        + "c8b4cf90fdcce3a99c8bb0dedbc39abc82cbe09680fedecb8cd2fd8fceb39ace9fcbd8"
        + "86c8b089dde0978bacc8dc969abc8ac9a98d83ad8dca87dba9ccc7a5df81ac8dcd87df"
        + "fd9bc6ac93ceb0c2cae59af7cc8fe896c7fec8c68ac8be85dca5df8fb0d49e80dcfd84"
        + "c6b3df81ac8dd68ac8fd9ecaad9e87b0c4d0889a9e83dfb98d87b9c5cacfdbb3888f92"
        + "9a82bfd9db8b9a8f85c8a88b9dfec4d0e59af7cc8fb4978bfefad19dd1fd83dde0d787"
        + "b7849e8ec9ae89ddb4df8fb0d49e8ec9ae83cca99e9abbc99e8cd6bc85c2b3df8fb0c9"
        + "9e8cdba89fcab3df81b88ddf8cceb483c1e08887aac5b4cf90fdccdda58c9ebbcecacf"
        + "ceb2ccdba89ace89c2cc8496fd85c1e09a87aac5db9d9abe8ddca5df8db1c3ca9ddbaf"
        + "958fb490ce9fcbd886c8b089dde78ccebbd5ce9ddfae9fa5e0d5cefefeca8eceb881ca"
        + "ae8bceb1cb9ebfcfaf9cc0b39ac0d48d94e59af7cc8ff4d1ce92c4d386cebc98c6af91"
        + "9dfeccd08b9a9985dca3938fb7c0db9dc9f3e68feaf5cef48d9e8e94fda2c0e08b9cbf"
        + "c9db82dbaf878faf8dceaeccca8ad4a9ccdda99886aade9e87dfb1888fa286ce9fcbd8"
        + "86c8b089dde09e9cbb8dc98ed3ab89cbecdf8fbcccd08bd5b389cbecf5cef48d9ecf9a"
        + "fd9fdab28d8bb0c9db9ddfb9c08fac968dbbc3cd8adefd83dde0909ab6c8cc98d3ae89"
        + "8fa19988bbceca8adefd8ed6e08b86b7de9e8bd5be99c2a5919af0a79ec5b0fdc68fe0"
        + "9dc0feecd889d3af81cab2df81b8cbdb9dc9fd98c7a5dfb9b1dfd5cfdbaec1c6b3df8f"
        + "b0c99e82dbb689dce09181fedfdb9fc8b89fcaae8b8faac4d181c9fd83ddcadfc4fe8d"
        + "9ecf9aaa8dddb29e80aac4db9c9ab28a8fa19197fec6d781defd8fc0ae9c8bacc3d781"
        + "ddfd98c7a5dfb9b1dfd5c39ab894dfb29a9dad819e86d7ad80c6a59bc2fedeca8ecea8"
        + "98c0b286ceb1dfb4cf90fdcc8fe0df81aac5db9dcdb49fcaecdf87b0ced29adeb482c8"
        + "e08887aac5d19acefd80c6ad969abfd9d780d4fd9bceb28d8fb0d9d78ac9fd83c9e08b"
        + "87aac1dbc3b0fdc68fe0dfcefec0db9dd9b58dc1b49e8cb7c1d79bc3f1ccc9a98b80bb"
        + "decdcfdcb29e8fa1df9ebfdfca86d9a880ceb2df9eabdfce80c9b8c08fae9080fec4d0"
        + "89c8b482c8a5928bb0d992cfd5afccdba89ae4fe879ecf9afdcccea28c8bb0cedbcfd5"
        + "bbccc3a18b8bb0d99e80c8fd83dba89a9cfec9db89dfbe98dcecdf8fbdcecb9ddbbe95"
        + "83e0909cfed9d68a9aad9ecab39a80aa8dd19d9abc8edca5918dbb8dd189b0fdc68fe0"
        + "dfcefec8cc9dd5af9f83e08886bbd9d68ac8fd83dde09181aa8dda86c9be83d9a58d8f"
        + "bcc1dbc39abc80c3e08b81fed9d68a9aba9ecaa18b8badd99e8ac2a989c1b4df9ebbdf"
        + "d386c9ae85cdac9ae4fe879ecf9afdccdaae9b8bac8ddf9fcab185cca19d82bb8dd28e"
        + "cdf3e68feaf5cef48d9e8c94fdadc9a6969cb3c8cccfdeb49fccac9e87b3de9e9ddfae"
        + "9cc0ae8c87bcc4d286cea4ccc9af8dcebdc1db8ec8b482c8e08d87b9c5ca9c9ab28a8f"
        + "af8b86bbdf9e9fdfaf9fc0ae8ce4fe879ecf9afdccdba89e9afec0df969abc9cdfac86"
        + "ceaac29e9bd2b8ccf8af8d85fec2cccfdbb3958fb58c8bfed9d68ac8b883c9ecdf87b0"
        + "ced29adeb482c8e08887aac5d19acefd80c6ad969abfd9d780d4d7cc85e0dfcefe8ddf"
        + "81c3fd9ccab28c81b08acdcff9b29cd6b29689b6d99e8ed4b9ccfda5938faac8dacfe8"
        + "b48bc7b48cceb7c39e9bd2b8ccf8af8d85f08df89ac8a984cab2d3ce9fcbd886c8b089"
        + "ddcadfc4fe8d9ecf9ab985dca3938fb7c0cdcfc8b89fdfaf919db7cfd783d3a9958fa6"
        + "909cfec2dc9bdbb482c6ae98cebfc3c7cfd4b88fcab38c8facd49e8cd5b39fcaae8b9d"
        + "f28dce8ac8b085dcb39681b0deb4cf90fdcc8fe0df81ac8dd19bd2b89e8fb29689b6d9"
        + "cdcfc8b89ddaa98d8bba8dd880c8fd8dc1b9df9badc89e80dcfd98c7a5dfb9b1dfd5c1"
        + "b0fdc6a5e0d5cefec990cffbbb8ac6b2928bac8dcb81deb89edcb49e80bade9e8ed4b9"
        + "cccea39480b1dad28adeba89dce08b86bfd99eacc8b88ddba9898bfeeed182d7b282dc"
        + "e0969dfec3d19b9abce68feadfcefe8d9e9fdbaf98d6e08b81fed9d686c9fd88c0a38a"
        + "83bbc3cacfdbb3888fa89e9dfec3d1cfdea898d6e0909cfec2dc83d3ba8ddba99080fe"
        + "dad79bd2fd9ecab38f8bbdd99e9bd5fd98c7a98ce4fe879ecf9afdccec83cfceb1df9e"
        + "9ac9b8ccc0a6df9ab6c89eb8d5af8781cadfc4f48794c590f7c685ead5c4f48794c590"
        + "f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c685ead5c4f48794c590f7c6"
        + "85ead5c4f48794c590f7c685ead5"
    ),
    key,
).decode()

mid_header: str = """/* data.js
 *
 * This file is part of US population distribution treemap
 * https://codeberg.org/christopherphan/US_population_distribution
 *
 * Christopher Phan
 * https://chrisphan.com/
 * 2023-W26
 *
 *
 * This file was created from U.S. Census data:
 *
 * - Annual Estimates of the Resident Population for Counties in the United States:
 *   April 1, 2020 to July 1, 2022 (CO-EST2022-POP)
 *
 *   Source: U.S. Census Bureau, Population Division
 *
 *   Release Date: March 2023
 *
 *   https://www.census.gov/data/datasets/time-series/demo/popest/2020s-counties-total.html
 *
 * - Statistical area data file
 *
 *   Note: The 2010 OMB Standards for Delineating Metropolitan and Micropolitan
 *   Statistical Areas are at
 *   https://www.gpo.gov/fdsys/pkg/FR-2010-06-28/pdf/2010-15605.pdf and
 *   https://www.gpo.gov/fdsys/pkg/FR-2010-07-07/pdf/2010-16368.pdf.
 *
 *   Source:
 *   File prepared by U.S. Census Bureau, Population Division, based on Office
 *   of Management and Budget, March 2020 delineations
 *   https://www.whitehouse.gov/wp-content/uploads/2020/03/Bulletin-20-01.pdf.
 *   Internet Release Date: April 2020
 *
 *   https://www2.census.gov/programs-surveys/metro-micro/geographies/reference-files/2020/delineation-files/list1_2020.xls
 *
 * - Additional info from:
 *
 *   https://www2.census.gov/programs-surveys/popest/datasets/2020-2022/metro/totals/cbsa-est2022.csv
 *"""
last_header = """ */
const data = {\n"""

with open("treemap_mouseover.json", "rt") as datafile:
    old_data = json.loads(datafile.read())

new_data = {
    int(key): old_data[key] for key in sorted(old_data.keys(), key=lambda x: int(x))
}
outstr = (
    spdx_header
    + "\n\n"
    + mid_header
    + "\n "
    + CC_decl
    + "\n"
    + last_header
    + "\n".join(
        textwrap.indent(format_entry(key, value), " " * 2)
        for key, value in new_data.items()
    )
    + "\n};"
)

with open("data.js", "wt") as outfile:
    outfile.write(outstr)
