// SPDX-License-Identifier: Apache-2.0

/* US population treemap
 * src/lib.rs
 *
 * Christopher Phan
 * 2023-W26
 */

use csv::Reader;
use serde::{Deserialize, Serialize};
use serde_json;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::cmp::{Ord, Ordering, PartialOrd};
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::iter;
use std::rc::Rc;
use svg::{
    node::{
        element::{Rectangle, Style},
        Comment, Node,
    },
    Document,
};
use treemap::{MapItem, Mappable, Rect, TreemapLayout};

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum AreaType {
    CombinedStatArea,
    MetroStatArea,
    MicroStatArea,
    Root,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Area {
    County {
        index: usize,
        name: String,
        population: u64,
        parent: Rc<Area>,
    },
    StatArea {
        index: usize,
        name: String,
        area_type: AreaType,
        components: RefCell<Vec<Rc<Area>>>,
        parent: Option<Rc<Area>>,
    },
}

impl Area {
    pub fn parent(&self) -> Option<Rc<Area>> {
        match self {
            Self::County {
                index: _,
                name: _,
                population: _,
                parent,
            } => Some(parent.clone()),
            Self::StatArea {
                index: _,
                name: _,
                area_type: _,
                components: _,
                parent,
            } => parent.clone(),
        }
    }

    pub fn area_type_string(&self) -> String {
        match self {
            Self::County {
                index: _,
                name: _,
                population: _,
                parent: _,
            } => "County".to_string(),
            Self::StatArea {
                index: _,
                name: _,
                area_type,
                components: _,
                parent: _,
            } => match area_type {
                AreaType::CombinedStatArea => "CSA".to_string(),
                AreaType::MetroStatArea => "MSA".to_string(),
                AreaType::MicroStatArea => "\u{03bc}SA".to_string(),
                AreaType::Root => "root".to_string(),
            },
        }
    }

    pub fn index(&self) -> usize {
        match self {
            Self::County {
                index,
                name: _,
                population: _,
                parent: _,
            } => *index,
            Self::StatArea {
                index,
                name: _,
                area_type: _,
                components: _,
                parent: _,
            } => *index,
        }
    }

    pub fn name(&self) -> String {
        match self {
            Self::County {
                index: _,
                name,
                population: _,
                parent: _,
            } => name.clone(),
            Self::StatArea {
                index: _,
                name,
                area_type: _,
                components,
                parent: _,
            } => {
                if components.borrow().len() == 1 {
                    format!(
                        "{} ({})",
                        components.borrow().get(0).expect("has an element").name(),
                        name
                    )
                } else {
                    name.clone()
                }
            }
        }
    }

    pub fn population(&self) -> u64 {
        match self {
            Self::County {
                index: _,
                name: _,
                population,
                parent: _,
            } => *population,
            Self::StatArea {
                index: _,
                name: _,
                area_type: _,
                components,
                parent: _,
            } => components.borrow().iter().map(|k| k.population()).sum(),
        }
    }

    pub fn children(&self) -> Vec<Rc<Area>> {
        let mut out_vec = match self {
            Self::County {
                index: _,
                name: _,
                population: _,
                parent: _,
            } => vec![],
            Self::StatArea {
                index: _,
                name: _,
                area_type: _,
                components,
                parent: _,
            } => components.borrow().clone(),
        };
        out_vec.sort_by(|a, b| b.cmp(a));
        out_vec
    }

    pub fn add_component(&self, cmpt: Rc<Area>) {
        match self {
            Self::County {
                index: _,
                name: _,
                population: _,
                parent: _,
            } => { /* pass */ }
            Self::StatArea {
                index: _,
                name: _,
                area_type: _,
                components,
                parent: _,
            } => {
                components.borrow_mut().push(cmpt);
            }
        }
    }

    pub fn new_sa(
        name: String,
        data: &mut AreaData,
        area_type: AreaType,
        parent: Option<Rc<Area>>,
    ) -> Rc<Area> {
        let new_sa = Rc::new(Area::StatArea {
            index: data.allocate_index(),
            name: name.clone()
                + if area_type == AreaType::CombinedStatArea {
                    " CSA"
                } else {
                    ""
                },
            area_type: area_type.clone(),
            components: RefCell::new(Vec::new()),
            parent: parent.clone(),
        });
        data.insert(new_sa.clone());
        if let Some(p) = parent {
            p.add_component(new_sa.clone());
        }
        new_sa
    }

    pub fn new_county(name: String, population: u64, parent: Rc<Area>, data: &mut AreaData) {
        let new_county = Rc::new(Area::County {
            index: data.allocate_index(),
            name,
            population,
            parent: parent.clone(),
        });
        parent.add_component(new_county);
    }

    pub fn process_record(record: RawRecord, data: &mut AreaData) {
        let grandparent: Rc<Area> = if record.csa == "".to_string() {
            data.root.clone()
        } else {
            let csa_name = record.csa.clone() + " CSA";
            let result = data.csa_list.get(&csa_name);
            match result {
                Some(k) => k.clone(),
                None => Self::new_sa(
                    record.csa.clone(),
                    data,
                    AreaType::CombinedStatArea,
                    Some(data.root.clone()),
                ),
            }
        };
        let containing_msa_type: Option<AreaType> = if record.sa_type == "MSA".to_string() {
            Some(AreaType::MetroStatArea)
        } else if record.sa_type == "\u{03bc}SA" {
            Some(AreaType::MicroStatArea)
        } else {
            None
        };
        let parent: Rc<Area> = if let Some(t) = containing_msa_type {
            let result = data.msa_list.get(&record.stat_area);
            match result {
                Some(k) => k.clone(),
                None => Self::new_sa(record.stat_area.clone(), data, t, Some(grandparent)),
            }
        } else {
            data.root.clone()
        };
        Self::new_county(record.county, record.population, parent, data);
    }

    pub fn treemap(&self, bounds: Rect, level: usize) -> Vec<AreaInTreeMap> {
        let layout = TreemapLayout::new();
        let size = bounds.w * bounds.h;
        let mut mapitem_vec: Vec<Box<dyn Mappable>> = Vec::new();
        for child in self.children() {
            mapitem_vec.push(Box::new(MapItem::with_size(
                size * (child.population() as f64) / (self.population() as f64),
            )));
        }
        layout.layout_items(&mut mapitem_vec, bounds);
        let mut out_vec: Vec<AreaInTreeMap> = iter::zip(self.children(), mapitem_vec)
            .map(|(a, m)| AreaInTreeMap {
                area: a,
                map_item: m,
                level: level + 1,
            })
            .collect();
        let mut to_add: Vec<AreaInTreeMap> = Vec::new();
        for (idx, child) in self.children().iter().enumerate() {
            let grandchildren = child.children();
            if grandchildren.len() >= 2 {
                let old_bounds = out_vec
                    .get(idx)
                    .expect("idx < # children")
                    .map_item
                    .bounds()
                    .clone();
                let new_bounds = if let Ok(r) = rect_shrink(old_bounds.clone(), 1.0) {
                    r
                } else {
                    old_bounds
                };

                let mut grandkids_mapitems: Vec<AreaInTreeMap> =
                    child.treemap(new_bounds, level + 1);
                to_add.append(&mut grandkids_mapitems);
            }
        }
        out_vec.append(&mut to_add);
        out_vec
    }

    pub fn in_metro_sa(&self) -> bool {
        {
            match self {
                Self::County {
                    index: _,
                    name: _,
                    population: _,
                    parent,
                } => parent.in_metro_sa(),
                Self::StatArea {
                    index: _,
                    name: _,
                    area_type,
                    components: _,
                    parent,
                } => {
                    *area_type == AreaType::MetroStatArea
                        || if let Some(p) = parent {
                            p.in_metro_sa()
                        } else {
                            false
                        }
                }
            }
        }
    }

    pub fn in_micro_sa(&self) -> bool {
        {
            match self {
                Self::County {
                    index: _,
                    name: _,
                    population: _,
                    parent,
                } => parent.in_micro_sa(),
                Self::StatArea {
                    index: _,
                    name: _,
                    area_type,
                    components: _,
                    parent,
                } => {
                    *area_type == AreaType::MicroStatArea
                        || if let Some(p) = parent {
                            p.in_micro_sa()
                        } else {
                            false
                        }
                }
            }
        }
    }

    pub fn in_csa(&self) -> bool {
        {
            match self {
                Self::County {
                    index: _,
                    name: _,
                    population: _,
                    parent,
                } => parent.in_csa(),
                Self::StatArea {
                    index: _,
                    name: _,
                    area_type,
                    components: _,
                    parent,
                } => {
                    *area_type == AreaType::CombinedStatArea
                        || if let Some(p) = parent {
                            p.in_csa()
                        } else {
                            false
                        }
                }
            }
        }
    }

    pub fn ancestor_indicies(&self) -> Vec<usize> {
        if let Some(p) = self.parent() {
            let mut out_vec: Vec<usize> = p.ancestor_indicies();
            out_vec.push(p.index());
            out_vec
        } else {
            vec![]
        }
    }
}

impl PartialOrd for Area {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.population() == other.population() {
            self.index().partial_cmp(&other.index())
        } else {
            self.population().partial_cmp(&other.population())
        }
    }
}

impl Ord for Area {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct AreaData {
    pub next_index: usize,
    pub csa_list: HashMap<String, Rc<Area>>,
    pub msa_list: HashMap<String, Rc<Area>>,
    pub root: Rc<Area>,
}

impl AreaData {
    pub fn new() -> Self {
        Self {
            next_index: 1,
            csa_list: HashMap::new(),
            msa_list: HashMap::new(),
            root: Rc::new(Area::StatArea {
                index: 0,
                name: "root".to_string(),
                area_type: AreaType::Root,
                components: RefCell::new(Vec::new()),
                parent: None,
            }),
        }
    }

    pub fn allocate_index(&mut self) -> usize {
        let to_allocate = self.next_index;
        self.next_index += 1;
        to_allocate
    }

    pub fn insert(&mut self, item: Rc<Area>) {
        match item.borrow() {
            Area::County {
                index: _,
                name: _,
                population: _,
                parent: _,
            } => { /* Pass */ }
            Area::StatArea {
                index: _,
                name,
                area_type,
                components: _,
                parent: _,
            } => {
                match area_type {
                    AreaType::CombinedStatArea => {
                        self.csa_list.insert(name.clone(), item.clone());
                    }
                    AreaType::MetroStatArea => {
                        self.msa_list.insert(name.clone(), item.clone());
                    }
                    AreaType::MicroStatArea => {
                        self.msa_list.insert(name.clone(), item.clone());
                    }
                    AreaType::Root => { /* pass */ }
                }
            }
        }
    }
}

pub struct AreaInTreeMap {
    pub area: Rc<Area>,
    pub map_item: Box<dyn Mappable>,
    pub level: usize,
}

impl AreaInTreeMap {
    pub fn bottom(&self) -> bool {
        self.area.children().len() < 2
    }

    pub fn create_treemap(data: AreaData, bounds: Rect) -> Vec<Self> {
        data.root.treemap(bounds, 0)
    }

    pub fn to_json(&self) -> serde_json::Result<String> {
        MapElement::from(self).to_json()
    }

    pub fn to_mouseover_json(&self) -> (usize, Vec<MouseoverJSON>) {
        (
            self.index(),
            MouseoverJSON::make_mouseover_json(self.area.borrow()),
        )
    }

    pub fn in_metro_sa(&self) -> bool {
        self.area.in_metro_sa()
    }
    pub fn in_micro_sa(&self) -> bool {
        self.area.in_micro_sa()
    }
    pub fn in_csa(&self) -> bool {
        self.area.in_csa()
    }

    pub fn index(&self) -> usize {
        self.area.index()
    }

    pub fn ancestor_indicies(&self) -> Vec<usize> {
        self.area.ancestor_indicies()
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct MapElement {
    pub index: usize,
    pub name: String,
    pub area_type: String,
    pub level: usize,
    pub in_metro_sa: bool,
    pub in_micro_sa: bool,
    pub in_csa: bool,
    pub xpos: f64,
    pub ypos: f64,
    pub width: f64,
    pub height: f64,
    pub population: u64,
    pub ancestors: Vec<usize>,
    pub bottom: bool,
}

impl From<&AreaInTreeMap> for MapElement {
    fn from(aitm: &AreaInTreeMap) -> Self {
        let bds = aitm.map_item.bounds();
        Self {
            index: aitm.index(),
            name: aitm.area.name(),
            area_type: aitm.area.area_type_string(),
            level: aitm.level,
            in_metro_sa: aitm.in_metro_sa(),
            in_micro_sa: aitm.in_micro_sa(),
            in_csa: aitm.in_csa(),
            xpos: bds.x,
            ypos: bds.y,
            width: bds.w,
            height: bds.h,
            population: aitm.area.population(),
            ancestors: aitm.ancestor_indicies(),
            bottom: aitm.bottom(),
        }
    }
}

impl MapElement {
    pub fn to_json(&self) -> serde_json::Result<String> {
        serde_json::to_string(&self)
    }

    pub fn to_svg_box(&self) -> Rectangle {
        let css_class: String = (if self.area_type == "root" {
            "root"
        } else if self.area_type == "CSA" {
            "CSA"
        } else if self.area_type == "MSA" {
            "MSA"
        } else if self.area_type == "\u{03bc}SA" {
            "\u{03bc}SA"
        } else if self.area_type == "County" {
            "County"
        } else {
            ""
        })
        .to_string()
            + &(if self.bottom {
                " ".to_string()
                    + if self.in_metro_sa {
                        "MSA-"
                    } else if self.in_micro_sa {
                        "\u{03bc}SA-"
                    } else {
                        ""
                    }
                    + if self.in_csa { "CSA-" } else { "" }
                    + "bottom"
            } else {
                "".to_string()
            })
            + &format!(" level_{}", self.level);
        Rectangle::new()
            .set("id", format!("rect_{}", self.index))
            .set("x", self.xpos.to_string())
            .set("y", self.ypos.to_string())
            .set("width", self.width.to_string())
            .set("height", self.height.to_string())
            .set("class", css_class)
    }
}

pub fn rect_shrink(rect: Rect, border: f64) -> Result<Rect, &'static str> {
    rect_space_on_bottom(rect, border, border)
}

pub fn rect_space_on_bottom(
    rect: Rect,
    border: f64,
    bottom_space: f64,
) -> Result<Rect, &'static str> {
    if rect.w > 2.0 * border && rect.h > border + bottom_space {
        Ok(Rect {
            x: rect.x + border,
            y: rect.y + border,
            w: rect.w - 2.0 * border,
            h: rect.h - border - bottom_space,
        })
    } else {
        Err("Too small to shrink")
    }
}

pub fn make_svg(data: AreaData, bounds: Rect, svg_filename: &str, json_filename: &str) {
    let mut spdx = String::new();
    spdx.push('S');
    spdx.push('P');
    spdx.push('D');
    spdx.push('X');
    let spdx_ident = Comment::new(format!("{}-License-Identifier: CC-BY-4.0", spdx));
    let map_area = rect_space_on_bottom(bounds, 5.0, 15.0).unwrap();
    let areas_in_tm: Vec<AreaInTreeMap> = AreaInTreeMap::create_treemap(data, map_area);
    let roots = areas_in_tm.iter().filter(|k| k.bottom());
    let json_output: HashMap<usize, Vec<MouseoverJSON>> =
        HashMap::from_iter(roots.map(|k| k.to_mouseover_json()));
    fs::write(json_filename, serde_json::to_string(&json_output).unwrap()).unwrap();

    let style_data = Style::new(
        "rect { fill: none };\n".to_string()
            + "rect.root { stroke: none;}\n"
            + "rect.CSA { stroke: #ff3333; stroke-width: 3;}\n"
            + "rect.MSA { stroke: #009900; stroke-width: 2;}\n"
            + "rect.\u{03bc}SA { stroke: #009900; stroke-width: 2;}\n"
            + "rect.County { stroke: #69653c; stroke-width: 1; }\n"
            + "rect.MSA-CSA-bottom { fill: #55cccc;}\n"
            + "rect.\u{03bc}SA-CSA-bottom { fill: #77ffff;}\n"
            + "rect.MSA-bottom { fill: #cc77cc;}\n"
            + "rect.\u{03bc}SA-bottom { fill: #ffaaff;}\n"
            + "rect.highlight { fill: #ff9999; }",
    );

    let map_elts: Vec<MapElement> = areas_in_tm.iter().map(|k| MapElement::from(k)).collect();
    let level_1: Vec<MapElement> = map_elts.iter().filter(|k| k.level == 1).cloned().collect();
    let level_2: Vec<MapElement> = map_elts.iter().filter(|k| k.level == 2).cloned().collect();
    let level_3: Vec<MapElement> = map_elts.iter().filter(|k| k.level == 3).cloned().collect();
    let mut document = Document::new().set("viewBox", (bounds.x, bounds.y, bounds.w, bounds.h));
    document.append(spdx_ident);
    document.append(style_data);
    for k in level_3 {
        document.append(k.to_svg_box());
    }
    for k in level_2 {
        document.append(k.to_svg_box());
    }
    for k in level_1 {
        document.append(k.to_svg_box());
    }

    let mut authorship_msg = svg::node::element::Text::new()
        .set("x", 5.0)
        .set("y", bounds.h - 5.0)
        .set("font-family", "monospace");
    authorship_msg.append::<svg::node::Text>(svg::node::Text::new(
        "Copyright 2023 Christopher Phan. CC-BY-4.0".to_string()
            + " https://codeberg.org/christopherphan/US_population_distribution"
            + " \t Source: U.S. Census Bureau. Data current as of 2022.",
    ));
    document.append(authorship_msg);
    svg::save(svg_filename, &document).unwrap();
}

pub fn make_pdf(svg_filename: &str, pdf_filename: &str) {
    let s = std::fs::read_to_string(svg_filename).unwrap();

    let pdf = svg2pdf::convert_str(&s, svg2pdf::Options::default()).unwrap();

    std::fs::write(pdf_filename, pdf).unwrap();
}

#[derive(Debug, Eq, PartialEq, Clone, Deserialize)]
pub struct RawRecord {
    pub county: String,
    pub population: u64,
    pub sa_type: String,
    pub stat_area: String,
    pub csa: String,
}

pub fn get_data() -> Result<AreaData, Box<dyn Error>> {
    let mut data = AreaData::new();
    for record in Reader::from_path("combo_data.csv")?
        .deserialize::<RawRecord>()
        .map(|k| k.unwrap())
    {
        Area::process_record(record, &mut data);
    }
    Ok(data)
}

#[derive(Debug, Serialize, Clone)]
pub struct MouseoverJSON {
    pub index: usize,
    pub name: String,
    pub population: u64,
    pub area_type: String,
}

impl MouseoverJSON {
    pub fn make_mouseover_json(area: &Area) -> Vec<Self> {
        if area.area_type_string() == "root" {
            vec![]
        } else if let Some(p) = area.parent() {
            let mut out_vec = Self::make_mouseover_json(p.borrow());
            out_vec.push(MouseoverJSON {
                index: area.index(),
                name: area.name(),
                population: area.population(),
                area_type: area.area_type_string(),
            });
            out_vec
        } else {
            vec![]
        }
    }
}
