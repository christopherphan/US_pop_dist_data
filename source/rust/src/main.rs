// SPDX-License-Identifier: Apache-2.0

/* US population treemap
 * src/main.rs
 *
 * Christopher Phan
 * 2023-W26
 */
use treemap::Rect;
use us_pop_treemap;

fn main() {
    let d = us_pop_treemap::get_data().unwrap();
    us_pop_treemap::make_svg(
        d,
        Rect::from_points(0.0, 0.0, 1280.0, 800.0),
        "treemap.svg",
        "treemap_mouseover.json",
    );

    us_pop_treemap::make_pdf("treemap.svg", "treemap.pdf");
}
