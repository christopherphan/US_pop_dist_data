<!-- SPDX-License-Identifier: Apache-2.0 -->

# Making the U.S. population treemap

This repo contains the code necessary to produce the files `treemap.svg`, `treemap.pdf`,
and `data.js` in the repo at
[`https://codeberg.org/christopherphan/US_population_distribution`](https://codeberg.org/christopherphan/US_population_distribution).

## Preparing the CSV files

These are the steps used to obtain the CSV files in the `raw_data` directory.

### `raw_data/cbsa-est2022.csv`

1. Download [`https://www2.census.gov/programs-surveys/popest/datasets/2020-2022/metro/totals/cbsa-est2022.csv`](https://www2.census.gov/programs-surveys/popest/datasets/2020-2022/metro/totals/cbsa-est2022.csv)

### `raw_data/co-est2022-pop.csv`

1. Download
   [`https://www2.census.gov/programs-surveys/popest/tables/2020-2022/counties/totals/co-est2022-pop.xlsx`](https://www2.census.gov/programs-surveys/popest/tables/2020-2022/counties/totals/co-est2022-pop.xlsx)

2. Delete the first five rows and last five rows

3. Insert the following as the first row

   > County, 2020-04-01, 2020-07-01, 2021-07-01, 2022-07-01

4. Export as CSV

### `raw_data/list1_2020.csv`

1. Download
   [`https://www2.census.gov/programs-surveys/metro-micro/geographies/reference-files/2020/delineation-files/list1_2020.xls`](https://www2.census.gov/programs-surveys/metro-micro/geographies/reference-files/2020/delineation-files/list1_2020.xls)

2. Delete the first two rows and last four rows

3. Export as CSV

## Create the other files

1. Run the shell script `clean`

2. Run the shell script `build`

## Sources

- Annual Estimates of the Resident Population for Counties in the United States:
  April 1, 2020 to July 1, 2022 (CO-EST2022-POP)

  Source: U.S. Census Bureau, Population Division

  Release Date: March 2023

  https://www.census.gov/data/datasets/time-series/demo/popest/2020s-counties-total.html

- Statistical area data file

  Note: The 2010 OMB Standards for Delineating Metropolitan and Micropolitan
  Statistical Areas are at
  https://www.gpo.gov/fdsys/pkg/FR-2010-06-28/pdf/2010-15605.pdf and
  https://www.gpo.gov/fdsys/pkg/FR-2010-07-07/pdf/2010-16368.pdf.

  Source:
  File prepared by U.S. Census Bureau, Population Division, based on Office
  of Management and Budget, March 2020 delineations
  https://www.whitehouse.gov/wp-content/uploads/2020/03/Bulletin-20-01.pdf.
  Internet Release Date: April 2020

  https://www2.census.gov/programs-surveys/metro-micro/geographies/reference-files/2020/delineation-files/list1_2020.xls

- Additional info from:

  https://www2.census.gov/programs-surveys/popest/datasets/2020-2022/metro/totals/cbsa-est2022.csv
